% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/modeltest.R
\name{modeleval}
\alias{modeleval}
\title{Evaluates temp models}
\usage{
modeleval(primaryid, secondaryids, APItoken, weights = FALSE,
  threshold = 0.8, thresholdcolumn = "minR2", focaldf = NA)
}
\arguments{
\item{primaryid}{character, station id at the primary location}

\item{secondaryids}{character, station ids of secondary locations}

\item{APItoken}{character, token for downloading data through an API}

\item{weights}{numeric, predefined weights to use for model averaging}

\item{threshold}{numeric, minimum r^2 value for accepting a model to use for
prediction.}

\item{thresholdcolumn}{character, column to use that threshold in}

\item{focaldf}{data.frame, for if you dont want to use a primary id.}
}
\value{
A data.frame with the modeling results (R^2).
}
\description{
This function evaluates models that use temperatures from secondary location to predict temperatures at a primary location where it has missing values.
}

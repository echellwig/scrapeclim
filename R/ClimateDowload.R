#' @include dates.R preTclean.R tempclean.R NOAAdownload.R ucipmDownload.R
NULL


#' Downloads temperature data
#'
#' This function downloads temperature data from a variety of sources including
#'      NOAA, CIMIS, and IPM PestCasts. It then cleans up the data for use in
#'      models.
#'
#' @param loc ANY, location ID or if source is UCIPM lat/lon pair.
#' @param yrs numeric, years you want to download data for.
#' @param source character,
#' @param nearby data.frame, information about nearby stations. For NOAA it
#'     that come from the function meteo_nearby_stations() in the rnoaa
#'     package.
#' @param APItoken character, token for downloading data.
#' @param limit numeric, limit of things to download at a time
#' @param r2 numeric, minimum r squared value for models for filling in missing
#'     data holes.
#' @param stations numeric, number of nearby stations to investigate.
#' @param radius numeric, maximum radius to search for nearby stations.
#' @param modelweighting logical, should models with higher R^2 values be given
#'     more weight in the predictive models used to fill in gaps in the time
#'     series?
#' @param yearcols character, the name of the columns that contain the first
#'     and last years of the dataset.
#' @param stationvars character, variables in station metadata.
#' @param datavars character, variables in second batch of station metadata
#' @param tempvars character, variable in temperature data
#' @return A data.frame will all of the data that you want! or a list if there
#'     are still missing bits
#' @export
climateDownload <- function(loc, yrs, source, nearby=NA, APItoken, limit=50,
                            r2=0.85, stations=40, radius=100,
                            modelweighting=FALSE,
                             yearcols=c('first_year','last_year'),
                             stationvars=c('id','name','latitude',
                                           'longitude','distance'),
                             datavars=c('id','name','mindate','maxdate'),
                             tempvars=c('id','year','date','tmin','tmax')) {

    if (source=='UCIPM') {

        if (is.na(nearby)) {
            degrad <- radius/55.5
            nearby <- nearbyUCIPM(loc, yrs, degrad)
        }

        temps <-  ucipmDownload(loc, yrs, nearby)
        tclean <- tempclean(temps, loc, nearby$loc, dateform ='%Y-%m-%d')


    } else if (source=='NOAA') {
        stop('NOAA source is not yet implemented')

    } else if (source=='CIMIS') {
        stop('CIMIS source is not yet implemented')

    } else if (source=='Wunderground') {
        stop('Wunderground source is not yet implemented')

    } else {
        stop('Source must be either UCIPM, NOAA, CIMIS, or Wunderground')
    }





    return(temps)



}

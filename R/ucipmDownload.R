#' Find nearby UCIPM stations
#'
#' Finds stations near to the focal point and downloads the associated metadata.
#'
#' @param loc numeric, vector of length two with the lon and lat
#'     coordinates of the point of interest.
#' @param radius numeric, the distance a way from the point of interest you
#'     that you want use to predict climate (in degrees).
#' @param timeinterval numeric, vector of length two specifying the starting
#'     and ending years of interest.
#' @param incrementX increment for searching for meterological stations in the
#'     X (east-west) direction (in degrees longitude).
#' @param incrementY increment for searching for meterological stations in the
#'     Y (north-south) direction (in degrees latitude).
#' @return A data.frame with the variables code, lat, lon, start and end.
#' @export
nearbyUCIPM <- function(loc, radius, timeinterval, incrementX,
                        incrementY) {

    if (is.numeric(loc) | is.integer(loc)) {
        if (length(loc)==2) {
            xmin <- loc[1] - radius
            xmax <- loc[1] + radius
            ymin <- loc[2] - radius
            ymax <- loc[2] + radius
        } else {
            stop('loc must be an ordered pair of geographic coordinates.')
        }

    } else {
        stop('loc must be numeric or integer.')
    }


    Xs <- seq(xmin, xmax, by=incrementX)
    Ys <- seq(ymin, ymax, by=incrementY)

    XYs <-expand.grid(Xs,Ys)
    names(XYs) <- c('lon','lat')

    UCIPMstationlist <- mclapply(1:nrow(XYs), function(i) {
        handle_ucipm('list_stations', location=XYs[i,],
                     time_interval=timeinterval)
    }, mc.cores = 4L)

    UCIPMstationdf <- ldply(UCIPMstationlist, function(df) df)
    names(UCIPMstationdf) <- c('loc','lat','lon','start',
                               'end','dist','yr_ovrlp','perc_cov')

    UCIPMstns <- UCIPMstationdf[,-c(6:8)]
    UCIPMstns <- unique(UCIPMstns)

    return(UCIPMstns)
}




#' Downloads temperature data from UCIPM
#'
#' This function downloads temperature data from UCIPM database that consolidate
#' from a variety of places (including CIMIS, NOAA, and UC PestCast).
#'
#' @param loc numeric, lon/lat location of the focal point.
#' @param yearrange numeric, vector of length two specifying the start and end
#'      years of interest.
#' @param radius numeric, how far out from your focal point do you want use
#'     weather stations (in degrees).
#' @param nearby data.frame, a data.frame with the variables code, lat, lon,
#'     start and end. The data.frame can be created manually using the
#'     function `nearbyUCIPM()`. Alternatively, setting nearby to NA will
#'     cause the function to generate the data.frame.
#' @param lonlat logical, should the lon/lat data for each location be
#'     returned.
#' @param incrementX increment for searching for meterological stations in the
#'     X (east-west) direction (in degrees longitude).
#' @param incrementY increment for searching for meterological stations in the
#'     Y (north-south) direction (in degrees latitude).
#' @return A data.frame with the variables code, date, lat, lon, tmin, tmax.
ucipmDownload <- function(loc, yearrange, radius, nearby=NA, lonlat=FALSE,
                          incrementX=0.1, incrementY=0.05) {

    #
    if (is.na(nearby)) {
        nearbystnmeta <- nearbyUCIPM(loc, radius, incrementX, incrementY)

        nearby <- mclapply(1:nrow(nearbystnmeta), function(i) {
            handle_ucipm(action='list_stations',
                         location=nearbystnmeta[i, c('lon','lat')],
                         time_interval=yearrange)
        }, mc.cores = 4L)
    }

    UCIPMraw <- mclapply(1:nrow(nearbystnmeta), function(i) {
        get_weather(location = nearbystnmeta$loc[i],
                    station_list = nearby[[i]])$weather
    }, mc.cores = 4L)

    UCIPMdata <- mclapply(UCIPMraw, function(df) {
        datarows <- which(!is.na(df$Air.max))
        df2 <- df[datarows,c('Station','DATE','Air.max','min')]
        df2$date <- as.Date(df2$DATE)
        names(df2) <- c('loc','DATE','tmin','tmax','date')
        df2[,c('loc','date','tmin','tmax')]
    }, mc.cores = 4L)


    ucipmdf <- ldply(UCIPMdata, function(df) df)
    ucipmdf$year <- year(ucipmdf$date)
    ucipmdf <- ucipmdf[,c('loc','year','date','tmin','tmax')]


    if (lonlat) {
        locdf <- nearbystnmeta[,c('loc','lat','lon')]
        ucipmdf <- merge(ucipmdf, locdf, by='loc')
    }

    return(ucipmdf)

}

#' @include dates.R
NULL


#' Switches min and max temps
#'
#' This function switches minimum and maximum temperatures in the case that they are in the wrong columns
#'
#' @param x dataframe, or list of data.frames
#' @param vars character, the names of the minimum and maximimum temperature
#'     variables
#' @return A data.frame or list of data frames with the minimum and maximinum
#'     temperatures in the correct columns
#' @export
switchMinMax <- function(x, vars=c('tmin','tmax')) {
    #note the first one in the vars vector must be the smaller one

    if (is.data.frame(x)) {

        switchrows <- which(x[,vars[1]] > x[,vars[2]])
        switchdf <- data.frame(a=x[switchrows, vars[2]],
                              b=x[switchrows, vars[1]])
        x[switchrows, vars[1]] <- switchdf$a
        x[switchrows, vars[2]] <- switchdf$b

    } else if (is.list(x)) {

        xclass <- sapply(x, function(v) class(v))
        testclass <- rep('data.frame', length(x))


        if (identical(xclass, testclass)) {

            for (i in seq_along(x)) {
                xdf <- x[[i]]
                switchrows <- which(xdf[,vars[1]] > xdf[,vars[2]])
                switchdf <- data.frame(a=xdf[switchrows, vars[2]],
                                      b=xdf[switchrows, vars[1]])
                xdf[switchrows, vars[1]] <- switchdf$a
                xdf[switchrows, vars[2]] <- switchdf$b

                x[[i]] <- xdf
            }


        } else {
            stop('x must be a list of data.frames')
        }

    } else {
        stop('x must be a list or a data.frame')
    }

    return(x)

}

#' Reshapes NCDC data
#'
#' This function reshapes data downloaded using the function ghcnd() in the rnoaa package.
#'
#' @param df data.frame, result from download_ghcnd(), must have variables: id,
#'     year, month, and, element
#' @param vars character, names of the temperature variables (min and max)
#' @param valuename character, name of the value variable
#' @return A reshaped and column reduced data.frame with just the variables we want
ghcnd_reshape <- function(df, vars=c('TMIN','TMAX'), valuename='VALUE') {

    #print(head(df))

    valcols <- grep(valuename, names(df))

    dfr <- cbind(df[,c('id', 'year','month','element')], df[,valcols])

    #print(head(dfr))
    dfr <- dfr[dfr$element %in% vars, ]
    #print(head(dfr))
    dfmelt <- melt(dfr, id.vars = c('id', 'year','month','element'),
                   variable.name = 'day', value.name = 'temp')
    dfmelt$day <- as.character(dfmelt$day)

    #print(head(dfmelt))

    dfmelt$day <- gsub(valuename, '', dfmelt$day)
    dfmelt$day <- as.integer(dfmelt$day)

   # print(head(dfmelt))
    dfcast <- dcast(dfmelt, id + year + month + day ~ element,
                    value.var = 'temp')

    #print(head(dfcast))
    names(dfcast)[5:6] <- c('tmax','tmin')

    return(dfcast)

}

#' Dowloads NCDC data
#'
#' Downloads data from teh ncdc website using the function ghcnd().
#'
#' @param stations character, station ids you want to download data from.
#' @param APItoken character, the API token for downloading the data.
#' @return A data.frame with the dowloaded data
#' @export
ghncd_download <- function(stations, APItoken) {

    stnids <- paste0('GHCND:', stations)

    meta <- ldply(stnids, function(id) {
        voi <- c('longitude','latitude','elevation')
        ncdc_stations(stationid=id, token=APItoken)$data[,voi]
    })

    meta$id <- stations


    dailyT <- lapply(stations, function(id) {
        ghcnd(stationid=id, token=APItoken)
    })

    dailyt <- ldply(dailyT, function(dt) {
        ghcnd_reshape(dt)
    })

    dailyt <- switchMinMax(dailyt)



    dt <- merge(dailyt, meta)

    dt$date <- as.Date(paste(dt$year, dt$month, dt$day, sep='-'))

    dt$tmax <- dt$tmax/10
    dt$tmin <- dt$tmin/10



    return(dt)
}



#' Model missing temperatures
#'
#' This function uses temperatures from secondary locations to predict missing temperatures from a primary location using a linear model (lm).
#'
#' @param pr data.frame from primary location
#' @param pmiss character, missing dates in the primary time series
#' @param sr data.frame from the secondary location
#' @param smiss character, missing dates from the secondary location
#' @return A data frame with predicted temperature values for the missing dates
#'     for the primary location.
#' @export
modelMissing <- function(pr, pmiss, sr, smiss) {

    #print(str(pr))
    #print(str(sr))

    olp <- overlap(pr, sr)


   # print('in ModelMissing')
   # print(str(olp))
    tminmod <- lm(tmin ~ Stmin, data=olp)
    tmaxmod <- lm(tmax ~ Stmax, data=olp)

    sfound <- as.Date(setdiff(pmiss, smiss), origin='1970-01-01')

    ndat <- sr[sr$date %in% sfound, c('date','tmin','tmax')]
    names(ndat)[2:3] <- c('Stmin','Stmax')


    ndat$Ptmin <- predict(tminmod, newdata=ndat)
    ndat$Ptmax <- predict(tmaxmod, newdata=ndat)

    return(ndat)

}

#' Split column names
#'
#' Split the values of columns into two and take one side
#'
#' @param df data.frame with column to be split
#' @param name character, name of column to be split
#' @param extracols character, names of columns to be added to be kept around.
#' @return A data.frame with the the `name` column split and the `extracols` in
#'     the data.frame as well.
splitcols <- function(df, name, extracols='date') {

    colnums <- grep(name, names(df))
    colvars <- c(extracols, names(df)[colnums])

    splitdf <- df[, colvars]

    return(splitdf)

}

#' Organizing meta data
#'
#' This function organizes all metadata for our stations. Because NOAA couldn't just put it all in one place.
#'
#' @param x1 data.frame, first set of metadata
#' @param x2 data.frame, second set of metadata.
#' @param mergecol character, what column do `x1` and `x2` share? What variable
#'     should be used to merge them.
#' @param roundcols character, round these columns
#' @param namecol character, the name of the station
#' @param finalcols character, the columns that should end up in the final
#'     data.frame
#' @return A data.frame with all of the variables in finalcols
#' @export
metatemptable <- function(x1, x2, mergecol='id', roundcols=c('minR2','maxR2'),
                          namecol='name', finalcols=c('id','name','mindate',
                                                     'minR2','maxR2','range')) {

    x <- merge(x1, x2, by=mergecol)
    datecols <- grep('date', names(x), ignore.case = TRUE)

    for (i in datecols) {
        x[,i] <- as.Date(x[,i])
    }

    for (rc in roundcols) {
        x[,rc] <- round(x[,rc], 3)
    }

    x[,namecol] <- sapply( x[,namecol], function(str) {
        strsplit(str, ',')[[1]][1]
    })

    x$range <- as.integer(x[,'maxdate'] - x[,'mindate'])
    x$range <- ceiling(x$range/365)

    return(x[,finalcols])
}


#' Do time series overlap?
#'
#' Checks to see if two time series overlap
#'
#' @param prd data.frame, the primary data.frame time series
#' @param srd data.frame, the secondary data frame time series
#' @param row_requirement numeric, the minimum number of days the two time
#'     series must overlap. Default value is 10.
#' @return The times where they overlap
overlap <- function(prd, srd, row_requirement=10) {

    vnames <- names(prd)
    if ('id' %in% vnames) {
        voi <- c('id','tmin','tmax')
    } else if ('loc' %in% vnames) {
        voi <- c('loc','tmin','tmax')
    } else {
        stop('The location variable name must be id or loc.')
    }

    odate <- as.Date(intersect(srd[,'date'], prd[,'date']), origin="1970-01-01")

    x <- cbind(prd[prd$date %in% odate, ],
               srd[srd$date %in% odate, voi])

    names(x) <- c(names(prd), paste0("S", voi))

    if (nrow(x)<row_requirement) {
        return(NA)
    } else {
        return(x)
    }
}









